const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./src/**/*.{ts,tsx}', './public/index.html'],
  darkMode: false,
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: colors.white,
      black: colors.black,
      red: colors.red,
      green: {
        ...colors.green,
        2: '#43B748'
      },
      primary: {
        ...colors.red,
        1: '#EF3340'
      },
      indigo: {
        DEFAULT: '#6769FF'
      },
      gray: {
        1: '#494F66',
        2: '#676F8F',
        3: '#A3ABCC',
        4: '#C5CBE0',
        5: '#E4E8F7',
        6: '#F0F2FA',
        7: '#F7F8FC',
        100: '#C5CBE0'
      },
      acian: {
        2: '#6F7DFF'
      }
    },
    fontFamily: {
      sans: "'Lato', sans-serif",
      roboto: "'Roboto', sans-serif"
    },
    fontSize: {
      xs: ['0.75rem', { lineHeight: '1rem' }],
      sm: ['0.875rem', { lineHeight: '1.5rem' }],
      base: ['1rem', { lineHeight: '1.5rem' }],
      lg: ['1.125rem', { lineHeight: '1.75rem' }],
      xl: ['1.25rem', { lineHeight: '1.75rem' }],
      '2xl': ['1.5rem', { lineHeight: '1.81rem' }],
      '3xl': ['2rem', { lineHeight: '3rem' }],
      'title-3': [
        '2.5rem',
        {
          lineHeight: '3rem',
          letterSpacing: '-0.6px'
        }
      ],
      'title-4': [
        '1.75rem',
        {
          lineHeight: '2.25rem',
          letterSpacing: '-0.6px'
        }
      ],
      'title-5': ['1.5rem', { lineHeight: '2rem', letterSpacing: '-0.2px' }],
      'title-6': ['1.25rem', { lineHeight: '1.75rem' }],
      'title-7': ['1rem', { lineHeight: '1.5rem', letterSpacing: '0.2px' }],
      'title-8': ['0.75rem', { lineHeight: '1rem', letterSpacing: '0.6px' }],
      'title-9': [
        '0.625rem',
        {
          fontWeight: 'bold',
          lineHeight: '1rem',
          letterSpacing: '0.8px'
        }
      ]
    },
    boxShadow: {
      sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
      DEFAULT:
        '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
      md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
      lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
      xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
      '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
      inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
      footer:
        '0px 1px 4px rgba(163, 171, 204, 0.06), 0px 8px 30px rgba(20, 43, 127, 0.1)',
      none: 'none'
    },
    extend: {
      zIndex: {
        '-1': -1,
        49: 49,
        60: 60,
        70: 70,
        80: 80,
        90: 90,
        99: 99,
        999: 999,
        9999: 9999,
        99999: 99999
      },
      maxWidth: (theme) => ({
        ...theme('spacing'),
        ...theme('extendedSpacing'),
        56: '224px'
      }),
      height: (theme) => ({
        ...theme('spacing'),
        ...theme('extendedSpacing'),
        'screen-wn': 'calc(100vh - 64px)'
      }),
      minHeight: (theme) => ({
        ...theme('spacing'),
        ...theme('extendedSpacing'),
        'screen-wn': 'calc(100vh - 64px)'
      })
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
