# Hacking Challenge

## Uso

Debes usar tu administrador de paquetes para instalar las dependencias.

```
# npm
npm install

# yarn
yarn
```

Para iniciar el proyecto deberas usar el script "dev"

```
# npm
npm run dev

# yarn
yarn dev
```

## Librerias

- Vite
- Axios
- Formik
- TailwindCSS

## Vite

Es una herramienta para Frontend, creada por Evan You, mismo creador de Vuejs, esta herramienta nos permitirá crear aplicaciones de Javascript usando algún framework o libreria como ReactJs, Vuejs, Angular ya sea con algún framework especifico o ya sea Vanillajs, se seleccione en este proyecto ya que al trabajar con typescript el servidor de desarrollo puede ir mas lento en herramientas como webpack.

Algunas de sus ventajas son:

- Rápido HMR
- Build Optimizado
- Inicio Instantáneo del Server
- APIs completamentamente tipadas

## Axios

Axios es una librería JavaScript para realizar peticiones HTTP desde Node o XMLHttpRequest o un navegador. Como biblioteca moderna, está basada en la API Promise. Axios tiene algunas ventajas, como la protección contra los ataques de falsificación de solicitudes entre sitios (CSFR).

Algunas de sus ventajas son:

- Conversion a JSON
- Codigo simplificado
- El manejo de errores es bastante fácil

## Formik

Formik es simple de utilizar porque se encarga de las cosas repetitivas y molestas, se encarga de realizar un seguimiento de los valores, errores, que campos visitamos, validar y manejar el envió, nos libramos de todas estas tareas tediosas y repetitivas. Asi nos enfocamos más en la lógica de negocio. Formik no utiliza bibliotecas externas de administración de estados como Redux o Mobx entre otras. Esto hace que Formik sea facil de adoptar.

## TailwindCSS

Tailwind CSS es una potente herramienta para el desarrollo frontend. Está dentro de la clasificación de los frameworks CSS o también llamados frameworks de diseño. Permite a los desarrolladores y diseñadores aplicar estilos a los sitios web de una manera ágil y optimizada.

Algunas de sus ventajas son:

- Utility-first
- Facíl de personalizar
- Facilidad para diseñar componentes
- Elimina estilos sin usar en el proyecto

## Partes mas complicadas de desarrollar

La parte mas complicada de desarrollar fue la sección de coberturas ya que el diseño en mobile es distinto al diseño desktop, poder darle funcionalidad mientras cambia en distintas pantalla llevo mas tiempo.
