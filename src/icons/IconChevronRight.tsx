import { SVGProps } from 'react'

const IconChevronRight = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width={16}
      height={16}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.253 3.239a.856.856 0 011.186 0l4.308 4.166a.825.825 0 010 1.19L6.439 12.76a.856.856 0 01-1.186 0 .825.825 0 010-1.189L8.946 8 5.253 4.428a.825.825 0 010-1.19z"
        fill="#fff"
      />
    </svg>
  )
}

export default IconChevronRight
