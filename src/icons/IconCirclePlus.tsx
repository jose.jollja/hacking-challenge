import { SVGProps } from 'react'

const IconCirclePlus = (props: SVGProps<SVGSVGElement>) => (
  <svg
    {...props}
    width={24}
    height={24}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      opacity={0.6}
      d="M12 0C5.383 0 0 5.383 0 12s5.383 12 12 12 12-5.383 12-12S18.617 0 12 0Zm0 23.077C5.892 23.077.923 18.107.923 12 .923 5.892 5.893.923 12 .923c6.108 0 11.077 4.97 11.077 11.077 0 6.108-4.97 11.077-11.077 11.077Z"
      fill="#939DFF"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M11.415 8.233a.596.596 0 1 1 1.192 0v3.282h3.444a.596.596 0 1 1 0 1.192h-3.444v3.607a.596.596 0 1 1-1.192 0v-3.607H7.97a.596.596 0 1 1 0-1.192h3.445V8.233Z"
      fill="#6F7DFF"
    />
  </svg>
)

export default IconCirclePlus
