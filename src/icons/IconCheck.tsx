import { SVGProps } from 'react'

const IconCheck = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width={11}
      height={9}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10.318.927a.75.75 0 01.088 1.057l-5.5 6.5a.75.75 0 01-1.118.031l-2.833-3a.75.75 0 111.09-1.03l2.258 2.39 4.958-5.86a.75.75 0 011.057-.088z"
        fill="#fff"
      />
    </svg>
  )
}

export default IconCheck
