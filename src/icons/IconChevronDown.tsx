import { SVGProps } from 'react'

const IconChevronDown = (props: SVGProps<SVGSVGElement>) => (
  <svg
    {...props}
    width={16}
    height={16}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12.761 5.253a.856.856 0 0 1 0 1.186l-4.166 4.308a.825.825 0 0 1-1.19 0L3.24 6.439a.856.856 0 0 1 0-1.186.825.825 0 0 1 1.189 0L8 8.946l3.572-3.693a.825.825 0 0 1 1.19 0Z"
      fill="currentColor"
    />
  </svg>
)

export default IconChevronDown
