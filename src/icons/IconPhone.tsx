import { SVGProps } from 'react'

const IconPhone = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width={17}
      height={20}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.83 11.333c-1.774-2.546-2.424-3.998-.79-5.147 1.404-.989 1.311-1.099.048-3.553C4.92.366 4.434-.22 2.827 1.082-.747 3.578.204 7.629 4.092 13.209c3.887 5.58 7.493 7.865 10.954 5.411 1.767-1.073 1.386-1.732-.335-3.61-1.863-2.032-1.952-2.133-3.333-1.161-1.61 1.132-2.774.031-4.548-2.516z"
        fill="#6769FF"
      />
    </svg>
  )
}

export default IconPhone
