import { SVGProps } from 'react'

const IconCircleMinus = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      {...props}
      width={24}
      height={24}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity={0.6}
        d="M12 0C5.383 0 0 5.383 0 12s5.383 12 12 12 12-5.383 12-12S18.617 0 12 0Zm0 23.077C5.892 23.077.923 18.107.923 12 .923 5.892 5.893.923 12 .923c6.108 0 11.077 4.97 11.077 11.077 0 6.108-4.97 11.077-11.077 11.077Z"
        fill="#939DFF"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.052 11.515a.596.596 0 1 1 0 1.192H7.97a.596.596 0 1 1 0-1.192h8.08Z"
        fill="#6F7DFF"
      />
    </svg>
  )
}

export default IconCircleMinus
