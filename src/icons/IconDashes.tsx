import { SVGProps } from 'react'

const IconDashes = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width={3}
      height={42}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        stroke="#9d9d9d"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeDasharray="2 8"
        d="M1.5 40.5v-39"
      />
    </svg>
  )
}

export default IconDashes
