import ProgressBars from '../ProgressBars'
import { IconBack } from '../../assets/icons/icons'

interface Props {
  stepper?: 1 | 2
  onGoBack?: () => void
}

const StepperMobile = ({ stepper = 1, onGoBack = () => {} }: Props) => {
  return (
    <div className="w-full flex items-center gap-3 px-8 py-3 mx-auto border-b border-gray-6 md:hidden">
      <button onClick={onGoBack} className="items-center gap-4">
        <IconBack className="text-gray-5" />
      </button>
      <span className="text-title-9 text-gray-2 font-bold">
        PASO {stepper} DE 2
      </span>
      <ProgressBars progressPercentage={stepper === 1 ? 25 : 100} />
    </div>
  )
}

export default StepperMobile
