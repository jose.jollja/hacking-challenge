const Footer = () => {
  return (
    <footer className="px-8 py-5 text-xs text-gray-3 border-t border-gray-5 font-light">
      © 2020 RIMAC Seguros y Reaseguros.
    </footer>
  )
}

export default Footer
