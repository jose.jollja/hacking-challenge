import { ReactNode } from 'react'
import { IconAlert } from '../../../icons'

interface Props {
  visible: boolean
  children: ReactNode
}

const FormError = ({ visible, children }: Props) => {
  if (!visible) return null

  return (
    <div className="flex items-center gap-2">
      <IconAlert className="w-4 h-4 text-red-400" />
      <span className="text-sm text-red-400">{children}</span>
    </div>
  )
}

export default FormError
