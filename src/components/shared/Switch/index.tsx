import { InputHTMLAttributes } from 'react'
import styles from './switch.module.css'

type Props = InputHTMLAttributes<HTMLInputElement> & {
  sizes?: 'lg'
}

const Switch = ({ id, className, sizes = 'lg', ...props }: Props) => {
  return (
    <div className={[styles.toggle, styles[sizes], className].join(' ')}>
      <input
        {...props}
        type="checkbox"
        id={`toggle-${id}`}
        className={styles.toggle_checkbox}
      />
      <label htmlFor={`toggle-${id}`} className={styles.toggle_label} />
    </div>
  )
}

export default Switch
