import { useField } from 'formik'
import { SelectHTMLAttributes } from 'react'
import { IconChevron } from '../../../icons'

import styles from './select.module.css'

interface Props extends SelectHTMLAttributes<HTMLSelectElement> {}

const Select = ({ children, ...props }: Props) => {
  const [field, { error, touched }] = useField(props.name!)

  return (
    <div className="relative">
      <select
        {...field}
        {...props}
        className={[styles.select, error && touched ? styles.error : ''].join(
          ' '
        )}
      >
        {children}
      </select>
      <IconChevron className="w-4 h-4 absolute top-5 right-3 text-primary-1" />
    </div>
  )
}

export default Select
