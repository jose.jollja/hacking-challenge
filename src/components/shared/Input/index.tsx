import type { InputHTMLAttributes } from 'react'
import { useField } from 'formik'

import styles from './input.module.css'
import FormError from '../FormError'

interface Props extends InputHTMLAttributes<HTMLInputElement> {}

const Input = ({ className, type = 'text', ...props }: Props) => {
  const [field, { error, touched }] = useField(props.name!)

  return (
    <div>
      <input
        {...field}
        {...props}
        className={[
          styles.input,
          error && touched ? styles.error : '',
          className
        ].join(' ')}
      />
      <FormError visible={!!error && touched}>{error}</FormError>
    </div>
  )
}

export default Input
