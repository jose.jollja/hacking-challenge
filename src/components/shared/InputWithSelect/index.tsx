import { useField } from 'formik'
import { InputHTMLAttributes, SelectHTMLAttributes } from 'react'
import { IconChevron } from '../../../icons'
import { Option } from '../../../interfaces'
import FormError from '../FormError'

import styles from './inputSelect.module.css'

interface Props {
  options?: Option[]
  classContainer?: string
  input?: InputHTMLAttributes<HTMLInputElement>
  select?: SelectHTMLAttributes<HTMLSelectElement>
}

const InputWithSelect = ({
  input,
  select,
  options = [],
  classContainer = ''
}: Props) => {
  const [fieldInput, { error, touched }] = useField(input!.name!)
  const [fieldSelect] = useField(select!.name!)

  return (
    <div>
      <div
        className={[
          styles.container,
          error && touched ? styles.error : '',
          classContainer
        ].join(' ')}
      >
        <div className="relative">
          <select {...fieldSelect} {...select}>
            {options.map(({ label, value }) => (
              <option key={`option-${value}`} value={value}>
                {label}
              </option>
            ))}
          </select>
          <IconChevron className="w-4 h-4 absolute top-5 right-3 text-primary-1" />
        </div>
        <input {...fieldInput} {...input} type="number" />
      </div>
      <FormError visible={!!error && touched}>{error}</FormError>
    </div>
  )
}

export default InputWithSelect
