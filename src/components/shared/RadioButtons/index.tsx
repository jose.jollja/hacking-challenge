import { useField } from 'formik'
import { HTMLAttributes } from 'react'
import { Option } from '../../../interfaces'

interface Props extends HTMLAttributes<HTMLDivElement> {
  name: string
  label?: string
  options?: Option[]
}

const RadioButtons = ({
  name = '',
  label = '',
  options = [],
  ...props
}: Props) => {
  const [field] = useField(name)

  return (
    <div {...props}>
      <span className="text-gray-1 ml-3">{label}</span>

      <div className="flex items-center gap-8">
        {options.map(({ label, value }) => (
          <div key={`option-${label}`}>
            <input
              {...field}
              name={name}
              type="radio"
              value={value}
              className="mr-3"
              id={`radio-${name}-${label}`}
            />
            <label htmlFor={`radio-${name}-${label}`} className="text-gray-1">
              {label}
            </label>
          </div>
        ))}
      </div>
    </div>
  )
}

export default RadioButtons
