import type { ButtonHTMLAttributes, ReactNode } from 'react'
import Spinner from '../Spinner'

import styles from './index.module.css'

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  size?: 'md' | 'sm'
  icon?: ReactNode
  isLoading?: boolean
  colorSchema?: 'primary'
}

const Button = ({
  icon,
  children,
  isLoading,
  className,
  size = 'md',
  colorSchema = 'primary',
  ...props
}: Props) => {
  return (
    <button
      {...props}
      disabled={isLoading}
      className={[
        styles.btn,
        styles[size],
        styles[colorSchema],
        className
      ].join(' ')}
    >
      {children}
      {isLoading ? <Spinner size="xs" /> : null}
      {icon}
    </button>
  )
}

export default Button
