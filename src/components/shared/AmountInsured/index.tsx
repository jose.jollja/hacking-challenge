import { HTMLAttributes } from 'react'

import Counter from '../../Counter'
import { formatCurrency } from '../../../utils/formatCurrency'

interface Props extends HTMLAttributes<HTMLDivElement> {
  min: number
  max: number
  value: number
  onChanges: (n: number) => void
}

const AmountInsured = ({
  min,
  max,
  value,
  onChanges = () => {},
  ...props
}: Props) => {
  const handlePlus = () => {
    if (value >= max) return
    onChanges(value + 100)
  }

  const handleMinus = () => {
    if (value <= min) return
    onChanges(value - 100)
  }

  return (
    <div {...props}>
      <div className="w-44">
        <p className="text-gray-1 mb-1">Indica la suma asegurada</p>
        <div className="flex items-center gap-3 mb-4 md:mb-0">
          <span className="text-gray-2 text-xs">MIN {formatCurrency(min)}</span>
          <span className="text-gray-5"> | </span>
          <span className="text-gray-2 text-xs">
            MAX {formatCurrency(max)}{' '}
          </span>
        </div>
      </div>

      <Counter
        onPlus={handlePlus}
        onMinus={handleMinus}
        count={formatCurrency(value)}
        className="mb-10 md:mb-0 w-full md:w-max"
      />
    </div>
  )
}

export default AmountInsured
