import { InputHTMLAttributes, ReactNode } from 'react'
import { useField } from 'formik'
import { IconCheck } from '../../../icons'

import styles from './checkbox.module.css'
import FormError from '../FormError'

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  children: ReactNode
}

const Checkbox = ({ name, children, className, ...props }: Props) => {
  const [field, { error, touched }] = useField(name!)

  return (
    <div className={className}>
      <div className="flex gap-4">
        <input {...field} id={`id-${name}`} type="checkbox" hidden />
        <label htmlFor={`id-${name}`} className={styles.checkbox_input}>
          {field.value ? <IconCheck /> : null}
        </label>
        <span className={styles.checkbox_label}>{children}</span>
      </div>
      <FormError visible={!!error && touched}> {error} </FormError>
    </div>
  )
}

export default Checkbox
