import styles from './spinner.module.css'

interface Props {
  size?: 'xs' | 'sm' | 'md'
}

const Spinner = ({ size = 'md' }: Props) => (
  <div className={['animate-spin', styles.spinner, styles[size]].join(' ')} />
)

export default Spinner
