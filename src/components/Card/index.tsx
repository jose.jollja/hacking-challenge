import { HTMLAttributes } from 'react'
import styles from './card.module.css'

interface Props extends HTMLAttributes<HTMLDivElement> {}

const Card = ({ children, className, ...props }: Props) => {
  return (
    <div {...props} className={[styles.card, className].join(' ')}>
      {children}
    </div>
  )
}

export default Card
