interface Props {
  progressPercentage?: 25 | 50 | 75 | 100
}

const ProgressBars = ({ progressPercentage = 25 }: Props) => {
  return (
    <div className="h-1 flex-1 bg-gray-5">
      <div
        style={{ width: `${progressPercentage}%` }}
        className={`h-full rounded-3xl ${
          progressPercentage > 20 ? 'bg-acian-2' : 'bg-gray-5'
        }`}
      ></div>
    </div>
  )
}

export default ProgressBars
