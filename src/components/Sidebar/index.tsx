import type { HTMLAttributes } from 'react'

import Footer from '../Footer'
import Navbar from '../Navbar'
import StepperMobile from '../StepperMobile'

import IconDashes from '../../icons/IconDashes'
import { IconBack } from '../../assets/icons/icons'

import styles from './sidebar.module.css'
import { StepperProps } from '../../interfaces'

interface Props extends HTMLAttributes<HTMLDivElement> {
  stepper?: 1 | 2
  stepperProps: StepperProps
}

const Sidebar = ({ children, stepper = 1, stepperProps, ...props }: Props) => {
  return (
    <div {...props}>
      <Navbar isBordered />
      <div className="flex">
        <div className="w-full sm:max-w-xs xl:max-w-sm bg-gray-7 flex-col items-center pt-28 hidden md:flex">
          <div className="w-48">
            <div
              className={[
                styles.sidebar_stepper,
                styles['sidebar_stepper-active']
              ].join(' ')}
            >
              <div>
                <span>1</span>
              </div>
              <span>Datos del auto</span>
            </div>

            <IconDashes className="self-start transform translate-x-3" />

            <div
              className={[
                styles.sidebar_stepper,
                stepper === 2
                  ? styles['sidebar_stepper-active']
                  : styles['sidebar_stepper-inactive']
              ].join(' ')}
            >
              <div>
                <span>2</span>
              </div>
              <span>Arma tu plan</span>
            </div>
          </div>
          {/*  */}
        </div>
        <div className="w-full min-h-screen-wn pt-16 md:pt-24 pb-10 xl:pl-24">
          <StepperMobile stepper={stepper} />
          <button
            onClick={stepper === 1 ? stepperProps.goLogin : stepperProps.prev}
            className="items-center gap-4 hidden md:flex mb-7 px-8 xl:px-0"
          >
            <IconBack className="text-primary-1" />
            <span className="text-gray-3 text-xs">VOLVER</span>
          </button>
          {children}
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default Sidebar
