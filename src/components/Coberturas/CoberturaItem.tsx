import { ReactNode } from 'react'
import Switch from '../shared/Switch'
import ToggleButton from '../ToggleButton'
import IconChevronDown from '../../icons/IconChevronDown'

import styles from './coberturas.module.css'

interface Props {
  id: 1 | 2 | 3
  title: string
  icon: ReactNode
  className?: string
  dropdownState: number
  onOpenDropDown: () => void
  onCloseDropDown: () => void
  onChange: (id: 1 | 2 | 3) => void
}

const CoberturaItem = ({
  id,
  icon,
  title,
  onChange,
  className,
  dropdownState,
  onOpenDropDown,
  onCloseDropDown
}: Props) => {
  const isOpen = id === dropdownState

  return (
    <div className={[styles.cobertura, className].join(' ')}>
      {icon}
      <div className="w-full">
        <div className="flex items-center mb-4">
          <p className={styles.title}>{title}</p>
          <div className="md:hidden ml-auto">
            <Switch id={`${id}`} onChange={() => onChange(id)} />
          </div>
          <div className="hidden md:block ml-auto">
            <button
              onClick={isOpen ? onCloseDropDown : onOpenDropDown}
              className={[styles.toggle, isOpen ? styles.toggle_open : ''].join(
                ' '
              )}
            >
              <IconChevronDown className="text-primary-1" />
            </button>
          </div>
        </div>

        <div className="hidden md:block">
          <ToggleButton id={`${id}`} onChange={() => onChange(id)} />
        </div>

        <p
          className={[
            styles.paragraph,
            isOpen ? styles.paragraph_open : ''
          ].join(' ')}
        >
          He salido de casa a las cuatro menos cinco para ir a la academia de
          ingles de mi pueblo (Sant Cugat, al lado de Barcelona) con mi bici, na
          llego a la academia que está en el centro del pueblo en una plaza
          medio-grande y dejo donde siempre la bici atada con una pitón a un
          sitio de esos de poner las bicis
        </p>

        <div className="md:hidden">
          <button
            className={[styles.toggle, isOpen ? styles.toggle_open : ''].join(
              ' '
            )}
            onClick={isOpen ? onCloseDropDown : onOpenDropDown}
          >
            <span>VER {isOpen ? 'MENOS' : 'MÁS'}</span>
            <IconChevronDown className="text-acian-2" />
          </button>
        </div>
      </div>
    </div>
  )
}

export default CoberturaItem
