import { useRef, useState } from 'react'
import CoberturaItem from './CoberturaItem'

import { IconTheft } from '../../icons'
import IconDamage from '../../icons/IconDamage'
import IconTotalLost from '../../icons/IconTotalLost'

interface Props {
  onChange: (total: number) => void
}

const coberturasPrices = {
  1: { price: 15 },
  2: { price: 20 },
  3: { price: 50 }
}

const Coberturas = ({ onChange }: Props) => {
  const coberturasSelected = useRef<number[]>([])
  const [dropdownState, setDropdownState] = useState(1)

  const handleClose = () => setDropdownState(0)

  const handleTotal = (id: 1 | 2 | 3) => {
    let total = 0
    const price = coberturasPrices[id].price
    const coberturas = coberturasSelected.current

    if (coberturasSelected.current.includes(price)) {
      coberturasSelected.current = coberturas.filter((n) => n !== price)
    } else {
      coberturasSelected.current = [...coberturas, price]
    }

    total = coberturasSelected.current.reduce((prev, item) => {
      return prev + item
    }, 0)

    onChange(total)
  }

  return (
    <div>
      <h3 className="text-title-6 mb-10 px-8">Agrega o quita coberturas</h3>

      {/* Header */}
      <div className="grid grid-cols-3 text-title-9 text-center md:px-8 xl:px-0">
        <button className="border-b-2 border-primary-1 cursor-pointer">
          <p className="text-primary-1 max-w-20 mx-auto mb-4 font-bold">
            PROTEGE A TU AUTO
          </p>
        </button>
        <button className="border-b-2 border-gray-5 cursor-pointer">
          <p className="max-w-24 mx-auto mb-4 font-bold">
            PROTEGE A LOS QUE TE RODEAN
          </p>
        </button>
        <button className="border-b-2 border-gray-5 cursor-pointer">
          <p className="max-w-14 mx-auto mb-4 font-bold">MEJORA TU PLAN</p>
        </button>
      </div>

      <div className="p-8 flex flex-col gap-6 ">
        <CoberturaItem
          id={1}
          icon={<IconTheft />}
          title="Llanta robada"
          dropdownState={dropdownState}
          onCloseDropDown={handleClose}
          onOpenDropDown={() => setDropdownState(1)}
          onChange={(id) => handleTotal(id)}
        />
        <CoberturaItem
          id={2}
          icon={<IconDamage />}
          title="Choque y/o pasarte la luz roja"
          dropdownState={dropdownState}
          onCloseDropDown={handleClose}
          onOpenDropDown={() => setDropdownState(2)}
          onChange={(id) => handleTotal(id)}
        />
        <CoberturaItem
          id={3}
          icon={<IconTotalLost />}
          title="Atropello en la vía Evitamiento"
          dropdownState={dropdownState}
          onCloseDropDown={handleClose}
          onOpenDropDown={() => setDropdownState(3)}
          onChange={(id) => handleTotal(id)}
        />
      </div>
    </div>
  )
}

export default Coberturas
