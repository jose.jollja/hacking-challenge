import { HTMLAttributes } from 'react'

import IconPlus from '../../icons/IconPlus'
import IconMinus from '../../icons/IconMinus'

import styles from './counter.module.css'

interface Props extends HTMLAttributes<HTMLDivElement> {
  count?: number | string
  onPlus?: () => void
  onMinus?: () => void
}

const Counter = ({
  count = 0,
  className,
  onPlus = () => {},
  onMinus = () => {},
  ...props
}: Props) => {
  return (
    <div {...props} className={[styles.counter, className].join(' ')}>
      <button type="button" onClick={onMinus}>
        <IconMinus />
      </button>
      <span>{count}</span>
      <button type="button" onClick={onPlus}>
        <IconPlus />
      </button>
    </div>
  )
}

export default Counter
