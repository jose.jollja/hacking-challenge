import { InputHTMLAttributes } from 'react'
import { IconCircleMinus, IconCirclePlus } from '../../icons'

import styles from './toggle.module.css'

interface Props extends InputHTMLAttributes<HTMLInputElement> {}

const ToggleButton = ({ id, className, ...props }: Props) => {
  return (
    <label
      htmlFor={`toggleButton-${id}`}
      className={[styles.toggle, className].join(' ')}
    >
      <input
        {...props}
        hidden
        type="checkbox"
        id={`toggleButton-${id}`}
        className={styles.input}
      />
      <IconCirclePlus className={styles.icon_add} />
      <IconCircleMinus className={styles.icon_remove} />
      <span className={styles.tag_add}>AGREGAR</span>
      <span className={styles.tag_remove}>QUITAR</span>
    </label>
  )
}

export default ToggleButton
