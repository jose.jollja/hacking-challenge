import Lottie from 'react-lottie'
import LoadingAnimation from '../../assets/animation/loading.json'

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: LoadingAnimation,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  }
}

const Loading = () => {
  return (
    <div className="fixed z-99999 h-screen w-full grid place-items-center">
      <div className="absolute w-full h-full bg-white bg-opacity-50" />
      <div className="w-96 h-96">
        <Lottie options={defaultOptions} />
      </div>
    </div>
  )
}

export default Loading
