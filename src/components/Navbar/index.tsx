import { Logo, IconPhone } from '../../icons'

import styles from './navbar.module.css'

interface Props {
  isBordered?: boolean
}

const Navbar = ({ isBordered }: Props) => {
  return (
    <nav
      className={[styles.navbar, isBordered ? styles.bordered : ''].join(' ')}
    >
      <div className="flex justify-between items-center px-8 py-5">
        <Logo />

        <div className="flex items-center gap-x-5">
          <div className="font-normal text-xs hidden md:block text-gray-2">
            ¿Tienes alguna duda?
          </div>
          <a
            href="tel:+51999111222"
            className="flex md:hidden items-center gap-2 text-indigo"
          >
            <IconPhone />
            Llámanos
          </a>
          <a
            href="tel:+51999111222"
            className="hidden md:flex items-center gap-2 text-indigo"
          >
            <IconPhone />
            (01) 411 6001
          </a>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
