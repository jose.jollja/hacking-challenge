import { Option } from '../interfaces'

export const documentTypes: Option[] = [
  { label: 'DNI', value: 'DNI' },
  { label: 'CE', value: 'CE' },
  { label: 'Pass', value: 'PASS' }
]
