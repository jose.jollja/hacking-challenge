import { BrowserRouter as Router } from 'react-router-dom'

import RootRouter from './router/RootRouter'
import AuthState from './context/auth/AuthState'

const App = () => {
  return (
    <AuthState>
      <Router>
        <RootRouter />
      </Router>
    </AuthState>
  )
}

export default App
