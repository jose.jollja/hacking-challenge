import axios from 'axios'
import { UserAPIResponse } from '../interfaces/UserAPIResponse'

const API = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com'
})

export const getUserByID = async (id: string) => {
  const res = await API.get<UserAPIResponse>(`/users/${id}`)
  return res.data
}
