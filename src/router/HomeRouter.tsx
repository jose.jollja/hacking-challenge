import { lazy } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
const HomePage = lazy(() => import('../pages/home/HomePage'))
const GraciasPage = lazy(() => import('../pages/home/GraciasPage'))

const HomeRouter = () => {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/gracias" component={GraciasPage} />
      <Redirect from="/auth/login" to="/" />
    </Switch>
  )
}

export default HomeRouter
