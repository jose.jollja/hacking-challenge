import { lazy } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

const LoginPage = lazy(() => import('../pages/auth/LoginPage'))

const AuthRouter = () => {
  return (
    <Switch>
      <Route path="/auth/login" component={LoginPage} />
      <Redirect from="/" to="/auth/login" />
    </Switch>
  )
}

export default AuthRouter
