import { Suspense } from 'react'
import Loading from '../components/Loading'
import { useAuth } from '../context/auth/AuthState'

import AuthRouter from './AuthRouter'
import HomeRouter from './HomeRouter'

const RootRouter = () => {
  const { isAuth } = useAuth()

  return (
    <Suspense fallback={<Loading />}>
      {isAuth ? <HomeRouter /> : <AuthRouter />}
    </Suspense>
  )
}

export default RootRouter
