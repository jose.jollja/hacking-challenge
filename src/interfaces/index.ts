export interface Formik {
  value: Record<string, string>
  errors: Record<string, string>
  touched: Record<string, string>
  handleBlur: () => void
  handleChange: () => void
}

export interface Option {
  label: string
  value: string | number
}

export interface User {
  user: string
  email: string
  plate: string
}

export interface AuthValue extends User {
  isAuth?: boolean
}

export interface LoginValues {
  phone: string
  plate: string
  terms: boolean
  nroDocument: string
  documentType: 'DNI' | 'CE' | 'PASS'
}

export interface Homevalues {
  year: string
  brand: string
  insured: number
  isGas: boolean | number
}

export interface StepperProps {
  next: () => void
  prev: () => void
  goLogin: () => void
  success: () => void
}
