import { Form, Formik, FormikProps } from 'formik'

import Button from '../../components/shared/Button'
import Select from '../../components/shared/Select'
import { useAuth } from '../../context/auth/AuthState'
import RadioButtons from '../../components/shared/RadioButtons'
import AmountInsured from '../../components/shared/AmountInsured'

import IconCar from '../../icons/IconCar'
import { Homevalues, StepperProps } from '../../interfaces'
import IconChevronRight from '../../icons/IconChevronRight'

const DatosAuto = ({ next }: StepperProps) => {
  const { user } = useAuth()

  return (
    <main className="w-full px-8 mt-10">
      <div className="mx-auto max-w-96 md:max-w-none">
        <h1 className="text-title-4 mb-4">
          ¡Hola, <span className="text-primary-1">{user}!</span>
        </h1>
        <h3 className="font-roboto text-gray-2 mb-8">
          Completa los datos de tu auto
        </h3>

        <Formik
          onSubmit={(v) => {
            console.log(v)
            next()
          }}
          initialValues={{
            year: '2019',
            isGas: false,
            insured: 14300,
            brand: 'Wolkswagen'
          }}
        >
          {(props: FormikProps<Homevalues>) => (
            <Form className="mx-auto flex gap-16">
              <div className="w-full max-w-96">
                <div className="flex flex-col gap-4 mb-6">
                  <Select name="year">
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                  </Select>
                  <Select name="brand">
                    <option value="BMW">BMW</option>
                    <option value="Wolkswagen">Wolkswagen</option>
                  </Select>
                </div>

                <div className="w-full bg-gray-7 rounded-lg mb-8 flex p-4 gap-4 lg:hidden">
                  <IconCar />

                  <div>
                    <p className="font-roboto text-gray-2">
                      ¿No encuentras el modelo?
                    </p>
                    <a href="#" className="text-indigo font-bold">
                      CLIC AQUÍ
                    </a>
                  </div>
                </div>

                <RadioButtons
                  name="isGas"
                  label="¿Tu auto es a gas?"
                  className="mb-8 flex flex-col sm:flex-row sm:items-center sm:justify-between gap-4"
                  options={[
                    { label: 'Si', value: 1 },
                    { label: 'No', value: 0 }
                  ]}
                />

                <AmountInsured
                  min={12500}
                  max={16500}
                  value={props.values.insured}
                  onChanges={(v) => props.setFieldValue('insured', v)}
                  className="md:flex md:items-center md:justify-between md:mb-10"
                />

                <Button
                  type="submit"
                  className="relative"
                  icon={<IconChevronRight className="absolute right-8" />}
                >
                  CONTINUAR
                </Button>
              </div>

              <div className="max-w-56 w-full hidden lg:block">
                <div className="border-b-2 border-gray-7 pb-4 mb-4">
                  <span className="text-xs text-gray-1 mb-4 font-bold">
                    AYUDA
                  </span>
                </div>

                <div className="flex items-center">
                  <p className="text-gray-1 mb-4">¿No encuentras el modelo ?</p>

                  <IconCar />
                </div>
                <a href="#" className="text-xs text-indigo font-bold">
                  CLIC AQUÍ
                </a>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </main>
  )
}

export default DatosAuto
