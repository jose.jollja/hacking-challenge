import { useState } from 'react'
import Card from '../../components/Card'
import { StepperProps } from '../../interfaces'
import Button from '../../components/shared/Button'
import Coberturas from '../../components/Coberturas'

import { formatCurrency } from '../../utils/formatCurrency'
import { IconCheck, IconEscudos } from '../../assets/icons/icons'
import IllustrationCoberturas from '../../assets/illustrations/IllustrationCoberturas'
import { useAuth } from '../../context/auth/AuthState'

const MONTO_BASE = 20

const CoberturaPage = ({ success }: StepperProps) => {
  const { plate } = useAuth()
  const [total, setTotal] = useState(MONTO_BASE)

  return (
    <div>
      <div className="w-full flex gap-10 xl:gap-24">
        <div className="w-full lg:max-w-96">
          {/* Detalle */}
          <div className="bg-gray-7 md:bg-white pt-10 md:pt-0 pb-14 md:pb-0 px-8 xl:px-0 mb-14 md:mb-16">
            <h2 className="text-title-4 md:text-title-3 text-gray-1 mb-2">
              Mira las coberturas
            </h2>
            <p className="font-roboto font-light text-gray-2 mb-6 md:mb-11">
              Conoce las coberturas para tu plan
            </p>
            <Card>
              <p className="text-gray-3 text-xs font-roboto">Placa: {plate}</p>
              <p className="w-40 text-gray-1 mb-4">Wolkswagen 2019 Golf</p>
              <button className="text-acian-2 text-title-9 font-bold cursor-pointer">
                EDITAR
              </button>
              <div className="absolute right-0 bottom-0">
                <IllustrationCoberturas />
              </div>
            </Card>
          </div>

          {/* Coberturas */}
          <Coberturas onChange={(t) => setTotal(MONTO_BASE + t)} />
        </div>

        {/* Precio */}
        <div className="w-full max-w-56 hidden lg:block mt-28">
          <div className="">
            <div className="pb-2 flex justify-between border-b-2 border-gray-5 mb-6">
              <div className="flex flex-col gap-1 text-title-5">
                {formatCurrency(total, 2)}
                <p className="text-xs font-roboto text-gray-2">mensuales</p>
              </div>
              <IconEscudos />
            </div>
            <div className="mb-8">
              <p className="text-base text-gray-1 mb-3">El precio incluye:</p>
              <div className="text-gray-2 flex flex-col gap-2">
                <div className="flex items-center gap-4">
                  <IconCheck />
                  <p className="font-roboto text-sm">Llanta de respuesto</p>
                </div>
                <div className="flex items-center gap-4">
                  <IconCheck />
                  <p className="font-roboto text-sm">Analisis de motor</p>
                </div>
                <div className="flex items-center gap-4">
                  <IconCheck />
                  <p className="font-roboto text-sm">Aros gratis</p>
                </div>
              </div>
            </div>
            <div className="w-full mt-4">
              <Button onClick={success} className="uppercase text-sm" size="sm">
                lo quiero
              </Button>
            </div>
          </div>
        </div>
      </div>
      {/* Precio mobile */}
      <div className="w-full lg:hidden fixed md:static bottom-0 px-8 py-3 bg-white shadow-footer sm:shadow-none">
        <div className="flex justify-around items-center gap-5">
          <div>
            <h4 className="text-title-5 text-gray-1 -mb-1">
              {formatCurrency(total, 2)}
            </h4>
            <span className="text-title-9 font-bold">MENSUAL</span>
          </div>

          <Button onClick={success} className="max-w-52">
            LO QUIERO
          </Button>
        </div>
      </div>
    </div>
  )
}

export default CoberturaPage
