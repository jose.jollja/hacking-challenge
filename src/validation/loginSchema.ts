import { LoginValues } from '../interfaces'
import { isEmpty } from '../utils'

export const loginSchema = (values: LoginValues) => {
  const errors: Record<keyof LoginValues, string | boolean> = {} as LoginValues

  // Basic
  if (isEmpty(values.phone)) {
    errors.phone = 'Este campo es obligatorio.'
  }
  if (isEmpty(values.plate)) {
    errors.plate = 'Este campo es obligatorio.'
  }
  if (values.nroDocument.length === 0) {
    errors.nroDocument = 'Este campo es obligatorio.'
  }
  if (!values.terms) {
    errors.terms = 'Debes aceptar los terminos y condiciones.'
  }

  // Especific
  if (values.phone.trim().length !== 9) {
    errors.phone = 'El número de celular debe tener 9 digitos.'
  }
  if (values.plate.length !== 7 || !values.plate.includes('-')) {
    errors.plate = 'Debes ingresar una placa valida (ABC-123). '
  }

  if (values.documentType === 'DNI') {
    const length = 8
    if (String(values.nroDocument).length !== length) {
      errors.nroDocument = `El número de documento debe tener ${length} digitos.`
    }
  } else {
    const length = 12
    if (String(values.nroDocument).length !== length) {
      errors.nroDocument = `El número de documento debe tener ${length} digitos.`
    }
  }

  return errors
}
