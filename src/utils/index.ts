export const isEmpty = (str: string) => String(str).trim().length === 0
