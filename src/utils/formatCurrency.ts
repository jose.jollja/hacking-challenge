export const formatCurrency = (n: number, decimals: number = 0) => {
  return new Intl.NumberFormat('en-US', {
    currency: 'USD',
    style: 'currency',
    maximumFractionDigits: decimals
  }).format(n)
}
