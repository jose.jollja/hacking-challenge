import { Dispatch } from 'react'

import { Action } from './authReducer'
import { AuthValue, User } from '../../interfaces'

export const authActions = (_: AuthValue, dispatch: Dispatch<Action>) => ({
  loginAction: (user: User) => {
    console.log(user)
    dispatch({ type: 'LOGIN', payload: user })
  },
  logoutAction: () => {
    dispatch({ type: 'LOGOUT' })
  }
})
