import { createContext, ReactNode, useContext, useReducer } from 'react'

import { authReducer } from './authReducer'
import { authActions } from './authActions'
import { initialState } from './initialState'

import { AuthValue, User } from '../../interfaces'

interface Props {
  children: ReactNode
}

interface Return extends AuthValue {
  loginAction: (user: User) => void
  logoutAction: () => void
}

const AuthContext = createContext({} as Return)

const AuthState = ({ children }: Props) => {
  const [state, dispatch] = useReducer(authReducer, initialState)
  return (
    <AuthContext.Provider value={{ ...state, ...authActions(state, dispatch) }}>
      {children}
    </AuthContext.Provider>
  )
}

export const useAuth = () => useContext(AuthContext)

export default AuthState
