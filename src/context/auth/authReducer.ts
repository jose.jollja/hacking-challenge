import { AuthValue, User } from '../../interfaces'
import { initialState } from './initialState'

export type Action = { type: 'LOGOUT' } | { type: 'LOGIN'; payload: User }

export const authReducer = (state: AuthValue, action: Action): AuthValue => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...action.payload,
        isAuth: true
      }

    case 'LOGOUT':
      return initialState

    default:
      return state
  }
}
