import { AuthValue } from '../../interfaces'

export const initialState: AuthValue = {
  user: '',
  email: '',
  plate: '',
  isAuth: false
}
