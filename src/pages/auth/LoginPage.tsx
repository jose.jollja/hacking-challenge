import { useState } from 'react'
import { Formik, Form, FormikProps, FormikHelpers } from 'formik'

import Navbar from '../../components/Navbar'
import Input from '../../components/shared/Input'
import Button from '../../components/shared/Button'
import { useAuth } from '../../context/auth/AuthState'
import Checkbox from '../../components/shared/Checkbox'
import InputWithSelect from '../../components/shared/InputWithSelect'
import IllustrationLogin from '../../assets/illustrations/IllustrationLogin'
import IllustrationLoginDesktop from '../../assets/images/IllustrationLogin.png'

import { getUserByID } from '../../api'
import type { LoginValues } from '../../interfaces'
import { documentTypes } from '../../data/documentTypes'
import { loginSchema } from '../../validation/loginSchema'

const initialValues: LoginValues = {
  phone: '',
  plate: '',
  terms: false,
  nroDocument: '',
  documentType: 'DNI'
}

const LoginPage = () => {
  const { loginAction } = useAuth()
  const [loading, setLoading] = useState(false)

  const handleSubmit = async (
    values: LoginValues,
    helpers: FormikHelpers<LoginValues>
  ) => {
    try {
      setLoading(true)
      console.log(values.nroDocument)
      const res = await getUserByID(String(values.nroDocument)[0])
      helpers.resetForm()
      setLoading(false)
      loginAction({
        user: res.name,
        email: res.email,
        plate: values.plate
      })
    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  return (
    <main className="pb-10 md:grid grid-cols-2 md:pb-0 h-screen">
      <Navbar />
      <div className="flex bg-gray-7 md:hidden items-center pt-10 mb-10">
        <div className="ml-8 w-52">
          <span className="text-title-9 text-gray-1 mb-2">¡NUEVO!</span>
          <h1 className="text-title-4 text-gray-1 mb-3">
            Seguro Vehicular <span className="text-primary-1">Tracking</span>
          </h1>
          <p className="text-sm text-gray-2 font-roboto">
            Cuentanos donde le haras seguimiento a tu seguro
          </p>
        </div>

        <IllustrationLogin className="ml-auto transform translate-y-7" />
      </div>

      <div className="w-full h-full relative hidden md:block">
        <img
          alt="Illustration"
          src={IllustrationLoginDesktop}
          className="absolute inset-0 w-full h-full object-cover"
        />
      </div>

      <div className="mx-8 sm:w-72 sm:mx-auto grid place-items-center">
        <div>
          <h2 className="text-title-5 text-gray-1 mb-6">Déjanos tus datos</h2>
          <Formik
            validate={loginSchema}
            onSubmit={handleSubmit}
            initialValues={initialValues}
          >
            {(props: FormikProps<LoginValues>) => (
              <Form>
                <div className="flex flex-col gap-4 mb-6">
                  <InputWithSelect
                    options={documentTypes}
                    select={{ name: 'documentType' }}
                    input={{
                      name: 'nroDocument',
                      placeholder: 'Nro. de doc'
                    }}
                  />
                  <Input type="tel" name="phone" placeholder="Celular" />
                  <Input type="plate" name="plate" placeholder="Placa" />
                </div>
                <Checkbox name="terms" className="mb-8">
                  <span className="text-xs">
                    Acepto la Política de{' '}
                    <a href="#" className="underline font-bold">
                      Protecciòn de Datos Personales
                    </a>{' '}
                    y los{' '}
                    <a href="#" className="underline font-bold">
                      Términos y Condiciones.
                    </a>
                  </span>
                </Checkbox>

                <Button isLoading={loading} type="submit">
                  COTÍZALO
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </main>
  )
}

export default LoginPage
