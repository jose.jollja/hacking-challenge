import { useState } from 'react'
import { RouteComponentProps } from 'react-router'

import Sidebar from '../../components/Sidebar'
import DatosAuto from '../../sections/home/DatosAuto'
import Cobertura from '../../sections/home/Cobertura'
import { useAuth } from '../../context/auth/AuthState'

type Stepper = 1 | 2

const HomePage = ({ history }: RouteComponentProps) => {
  const { logoutAction } = useAuth()
  const [stepper, setStepper] = useState<Stepper>(1)

  const stepperProps = {
    next: () => setStepper(2),
    prev: () => setStepper(1),
    goLogin: () => logoutAction(),
    success: () => history.push('/gracias')
  }

  const steps = {
    1: { id: 1, component: <DatosAuto {...stepperProps} /> },
    2: { id: 2, component: <Cobertura {...stepperProps} /> }
  }

  return (
    <Sidebar stepper={stepper} stepperProps={stepperProps}>
      {steps[stepper].component}
    </Sidebar>
  )
}

export default HomePage
