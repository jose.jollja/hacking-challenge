import Navbar from '../../components/Navbar'
import Button from '../../components/shared/Button'
import {
  IllustrationGracias,
  IllustrationGraciasMobile
} from '../../assets/illustrations/IllustrationGracias'
import { useAuth } from '../../context/auth/AuthState'

const GraciasPage = () => {
  const { email } = useAuth()

  return (
    <div>
      <Navbar />
      <div className="mt-16 w-full flex justify-center lg:hidden">
        <div className="mx-auto">
          <IllustrationGraciasMobile width="360" className="" />
        </div>
      </div>
      <div className="flex flex-col lg:flex-row items-center justify-center px-8 py-8 lg:px-0  lg:pt-20 gap-y-6">
        <div className="w-6/12 hidden lg:block">
          <div className="bg-gray-7 h-screen relative">
            <div className="absolute -right-24 top-28">
              <IllustrationGracias className="ml-auto transform translate-y-60" />
            </div>
          </div>
        </div>
        <div className="max-w-sm lg:max-w-full lg:w-full ">
          <div className="flex flex-col items-center pt-7 lg:pt-32 lg:w-7/12 lg:mx-auto lg:h-screen gap-y-5 lg:gap-y-7">
            <h1 className="text-title-4 text-gray-1 lg:text-3xl ">
              <div className="text-primary-1 ">¡Te damos la bienvenida!</div>
              Cuenta con nosotros para proteger tu vehículo
            </h1>
            <p className="text-base font-roboto text-gray-2">
              Enviaremos la confirmación de compra de tu Plan Vehícular Tracking
              a tu correo:
              <div className="text-gray-1 font-roboto font-semibold">
                {email}
              </div>
            </p>
            <div className="w-full mt-5">
              <Button
                size="md"
                className=" uppercase text-xs font-sans leading-3"
              >
                cómo usar mi seguro
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default GraciasPage
